.SUFFIXES : .o .c .f90
.f90.o:
	gfortran -g -fno-underscoring  -c $<
.c.o:
	cc -g -c $<
prova.x: my_backtrace.o prova_backtrace.o
	gfortran -g prova_backtrace.o my_backtrace.o -o prova.x 
prova: prova.x

clean:
	rm *.x *.o; touch *.f90 *.c 
        
