#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>

int line_numbers (void **addresses,int n){ 
    char  comm[256];
    for (int i=0; i< n; i++ ) {
        sprintf(comm, "addr2line -p -f -e $COMMAND %p \n", addresses[i]); 
        system(comm);
    } 
}


int my_backtrace(char **buff) {
    void *addresses[100]; 
    char **strings; 
    int n,i;
    n = backtrace(addresses,100);
    strings = backtrace_symbols(addresses,n); 
    line_numbers(addresses,n);
    *buff =  (char*) strings; 
    return n ;
}
