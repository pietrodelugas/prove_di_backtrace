module backtrace
use,intrinsic :: iso_c_binding
implicit none 
  type (c_ptr)   :: cp_array = C_NULL_PTR
  integer        :: n = 0
  ! 
  interface
    function my_backtrace(p) bind(c, name = 'my_backtrace') result (n)
      use iso_c_binding
      integer(c_int)   :: n
      type(c_ptr)      :: p 
    end function 
    ! 
    function strlen(s)  bind(C, name = 'strlen') result (l)
       use iso_c_binding
       implicit none
       integer(c_int)       :: l 
       type(c_ptr),value    :: s
    end function strlen
  end interface
contains 
  subroutine clean_backtrace(fptr)
     use,intrinsic :: iso_c_binding
     type(c_ptr),pointer,optional  :: fptr(:)
     type(c_ptr),pointer           :: loc_fptr(:)
     ! 
     if ( present(fptr) ) fptr => NULL(fptr)
     if (c_associated(cp_array)) then 
        call c_f_pointer(cp_array, loc_fptr,[n])
        if (associated(loc_fptr) ) deallocate (loc_fptr) 
        cp_array = C_NULL_PTR
        n = 0  
     end if  
  end subroutine clean_backtrace
end module backtrace  
!         
program mino 
implicit none
  character(len=256)  :: backtrace(20)
  integer             :: i,n 
  call sub1(backtrace,n)
  !
  do i = 1, n
     print *, trim(backtrace(i)), i
  end do
  ! 
end program 

subroutine sub1(s, n_)
use,intrinsic :: iso_c_binding
use backtrace  
implicit none
  character(len=256)  :: s(20) 
  integer             :: n_
  ! 
  type(c_ptr),pointer :: cpp(:)
  integer             :: i  
  character(len=256),pointer  :: fptr
  ! 
  call sub2() 
  n_ = n
  call c_f_pointer(cp_array,cpp,[n])
  print *, 'ciao ', n
  do i = 1, n
     call c_f_pointer(cpp(i), fptr)
     s(i) = trim( fptr(1:strlen(cpp(i))))
  end do 
  call clean_backtrace(cpp)

end subroutine sub1

subroutine sub2() 
use,intrinsic ::  iso_c_binding
implicit none
  ! 
  integer               :: i 
  call sub3()   
end subroutine sub2

subroutine sub3() 
use,intrinsic ::  iso_c_binding
use backtrace, only: my_backtrace
implicit none
  type(c_ptr)   :: p 
  ! 
  integer               :: i 
  i = my_backtrace(p)
  print *, 'CIAO',i
  call sub4()   
end subroutine sub3

subroutine sub4() 
use,intrinsic ::  iso_c_binding
implicit none
  ! 
  integer               :: i 
  call sub5()   
end subroutine sub4

subroutine sub5() 
use,intrinsic ::  iso_c_binding
implicit none
  ! 
  integer               :: i 
  call sub6()   
end subroutine sub5



subroutine sub6() 
use,intrinsic ::  iso_c_binding
use backtrace 
implicit none 
  ! 
  integer               :: i 
  ! 
  !
  !
  n = my_backtrace(cp_array)
  print '("trovato n che è ",I5)',n
end subroutine sub6





 

